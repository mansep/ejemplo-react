import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Row, Col } from "react-bootstrap";
import "./register.css";

export default class Register extends Component {
  render() {
    return (
      <Container>
        <Row className="mt-5">
          <Col md={{ span: 6, offset: 3 }}>
            <Card className="p-4 text-center">
              <img
                className="logo text-center"
                src="https://solarimpulse.com/files/companies/logo/2018-11-30-100722/logo_Algo_Paint.jpg"
                alt="logo"
              />
              <h2 className="mt-2">Registrar</h2>
              <Form className="ml-5 mr-5">
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Ingresa tu RUT</Form.Label>
                  <Form.Control
                    className="text-center"
                    type="text"
                    placeholder="12.345.678-9"
                  />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Ingresa tu Contraseña</Form.Label>
                  <Form.Control
                    className="text-center"
                    type="password"
                    placeholder="*********"
                  />
                </Form.Group>
                <Button variant="primary" type="button" block onClick={() => {alert('algo')} }>
                  Go !
                </Button>

                <Button variant="light" type="button" block href="/login">
                  Ya tengo cuenta
                </Button>
              </Form>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
