import React from 'react';
import './App.css';
import RouterApp from './router';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-svg-core/styles.css'

function App() {
  return (
    <div>
        <RouterApp />
    </div>
  );
}

export default App;
